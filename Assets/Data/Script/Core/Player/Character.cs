﻿using Data.Script.Core.Scenes;
using Data.Script.Core.Ships;
using Data.Script.Inputs;
using UnityEngine;

namespace Data.Script.Core.Player
{
    public class Character : BaseObject, ICharacter
    {
        [SerializeField] private string namePlayer;
        [SerializeField] private Ship currentShip;
        public string Name { get => namePlayer; set => namePlayer = value; }
        public Ship CurrentShip { get => currentShip; set => currentShip = value; }

        public override void BaseUpdate()
        {
            var xzDirection = SceneManager.Instance.Inputs.getMoveDirection;
            var yDirection = SceneManager.Instance.Inputs.getMoveYDirection;
            var rollDirection = SceneManager.Instance.Inputs.getRollDirection;
            var delta = SceneManager.Instance.Inputs.getDeltaDirection;
            currentShip.SpeedControll(xzDirection, yDirection);
            currentShip.FlyXZ(xzDirection);
            currentShip.FlyY(yDirection);
            currentShip.Roll(rollDirection);
            currentShip.YawPitch(delta);
        }
    }
}
