﻿using UnityEngine;

namespace Data.Script.Core.Model
{
    [System.Serializable]
    public struct ShipHealthModel
    {
        [SerializeField] private float _maxHealth;
        [SerializeField] private float _currentHealth;

        public float MaxHealth => _maxHealth;

        public float CurrentHealth => _currentHealth;

        public ShipHealthModel(float maxHealth)
        {
            _maxHealth = maxHealth;
            _currentHealth = maxHealth;
        }
        
        public ShipHealthModel(float maxHealth, float currentHealth) : this(maxHealth)
        {
            _currentHealth = currentHealth;
        }
        
        public ShipHealthModel(ShipHealthModel model)
        {
            _maxHealth = model.MaxHealth;
            _currentHealth = model.CurrentHealth;
        }
    }
}
