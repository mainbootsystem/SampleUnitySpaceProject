﻿using Data.Script.Core.Ships;
using UnityEngine;

namespace Data.Script.Core.Model
{
    [System.Serializable]
    public struct ShipModel
    {
        [SerializeField] private string _shipName;
        [SerializeField] private ShipHealthModel _health;
        [SerializeField] private ShipMovomentModel _move;
        [SerializeField] private ClassShip _shipClass;

        public string ShipName => _shipName;

        public ShipHealthModel Health => _health;

        public ShipMovomentModel Move => _move;

        public ClassShip ClassShip => _shipClass;
        
        public ShipModel(string shipName, ShipHealthModel health, ShipMovomentModel move, ClassShip shipClass)
        {
            _shipName = shipName;
            _health = health;
            _move = move;
            _shipClass = shipClass;
        }

        public ShipModel(ShipModel model)
        {
            _shipName = model.ShipName;
            _health = model.Health;
            _move = model.Move;
            _shipClass = model.ClassShip;
        }
        
        public ShipModel(ShipModel model, ShipHealthModel healthModel, ShipMovomentModel moveModel)
        {
            _shipName = model.ShipName;
            _health = healthModel;
            _move = moveModel;
            _shipClass = model.ClassShip;
        }
    }
}