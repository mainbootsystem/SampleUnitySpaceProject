﻿using UnityEngine;

namespace Data.Script.Core.Model
{
    [System.Serializable]
    public struct ShipMovomentModel
    {
        [SerializeField] private float _maxSpeed;
        [SerializeField] private float _currentSpeed;
        [SerializeField] private float _accelerationForce;
        [SerializeField] private float _brakingForce;
        [SerializeField] private float _yawPitchSpeed;

        public float MaxSpeed => _maxSpeed;

        public float CurrentSpeed
        {
            get => _currentSpeed;
            set => _currentSpeed = value;
        }

        public float AccelerationForce
        {
            get => _accelerationForce;
            set => _accelerationForce = value;
        }

        public float BrakingForce
        {
            get => _brakingForce;
            set => _brakingForce = value;
        }

        public float YawPitchSpeed
        {
            get => _yawPitchSpeed;
            set => _yawPitchSpeed = value;
        }
        
        public ShipMovomentModel(float maxSpeed, float accelerationForce, float brakingForce, float yawPitchSpeed)
        {
            _maxSpeed = maxSpeed;
            _accelerationForce = accelerationForce;
            _brakingForce = brakingForce;
            _yawPitchSpeed = yawPitchSpeed;
            _currentSpeed = 0;
        }
        
        public ShipMovomentModel(float maxSpeed, float currentSpeed, float accelerationForce, float brakingForce, float yawPitchSpeed) : this(maxSpeed, accelerationForce, brakingForce, yawPitchSpeed)
        {
            _currentSpeed = currentSpeed;
        }
        
        public ShipMovomentModel(ShipMovomentModel model)
        {
            _maxSpeed = model.MaxSpeed;
            _accelerationForce = model.AccelerationForce;
            _brakingForce = model.BrakingForce;
            _yawPitchSpeed = model.YawPitchSpeed;
            _currentSpeed = model.CurrentSpeed;
        }
    }
}