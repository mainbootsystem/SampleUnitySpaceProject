﻿using System;
using Data.Script.Core.Model;
using Data.Script.Core.Scenes;
using UnityEngine;
using UnityEngine.Events;

namespace Data.Script.Core.Ships
{
    public class Ship : BaseObject, IShip, IFly, IDamageShipAndComponent
    {
        [SerializeField] private ShipModel _ships;
        [SerializeField] private UnityEvent OnDestroed;
        [SerializeField] private UnityEvent OnTakeDamage;
        private bool isInverse;


        public ShipModel ShipInstance
        {
            get => _ships;
            private set => _ships = value;
        }

        public UnityEvent OnDestroedEvent { get => OnDestroed; set => OnDestroed = value; }
        public UnityEvent OnTakeDamageEvent { get => OnTakeDamage; set => OnTakeDamage = value; }
        public float SpeedLimmited { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override void BaseAwake()
        {
            InitializationShip();
        }

        #region IShip
        public virtual void InitializationShip() { }

        public void Destroed()
        {
            var health = Math.Round(ShipInstance.Health.CurrentHealth, 1);
            if (health <= 0.1f)
            {
                OnDestroed?.Invoke();
            }
        }

        public void SetDamage(float damage)
        {
            float tempHealth = ShipInstance.Health.CurrentHealth;
            tempHealth -= damage;
            
            ShipModel tempShipModel = new ShipModel(ShipInstance.ShipName,
                new ShipHealthModel(ShipInstance.Health.MaxHealth, tempHealth),
                ShipInstance.Move, ShipInstance.ClassShip);

            ShipInstance = new ShipModel(tempShipModel);
            
            OnTakeDamage?.Invoke();
            Destroed();
        }
        #endregion

        #region IFly
        public void FlyXZ(Vector2 direction)
        {
            if(direction.sqrMagnitude <= 0.1f) return;
            
            float scaleMoveSpeed = ShipInstance.Move.CurrentSpeed * Time.fixedDeltaTime;
            Vector3 move = Quaternion.Euler(0, transform.eulerAngles.y, 0) * new Vector3(direction.x, 0, direction.y);
            transform.position += move * scaleMoveSpeed;
        }

        public void FlyY(float direction)
        {
            float scaleMoveSpeed = ShipInstance.Move.CurrentSpeed * Time.fixedDeltaTime;
            var translate = Vector3.up * (direction * scaleMoveSpeed);

            transform.Translate(translate);
        }

        public void Roll(float direction)
        {
            transform.Rotate(0, 0, direction, Space.Self);
        }

        public void YawPitch(Vector2 delta)
        {
            if (delta.sqrMagnitude <= 0.1f) return;
            
            var scaledRotateSpeed = ShipInstance.Move.YawPitchSpeed * Time.fixedDeltaTime * delta;
            scaledRotateSpeed.y = isInverse ? scaledRotateSpeed.y : scaledRotateSpeed.y * -1;
            var euler = new Vector3(scaledRotateSpeed.y, scaledRotateSpeed.x, 0);
            transform.Rotate(euler);
        }

        public void SpeedControll(Vector2 xzDirection, float yDirection)
        {
            if (SceneManager.Instance.Pause.IsPaused) return;
            
            float tempCurrentSpeed = 0;
            if (xzDirection.sqrMagnitude > 0.1f || yDirection != 0)
            {
                tempCurrentSpeed = (ShipInstance.Move.CurrentSpeed < ShipInstance.Move.MaxSpeed) ? ShipInstance.Move.CurrentSpeed + ShipInstance.Move.AccelerationForce : ShipInstance.Move.MaxSpeed;
            }
            else
            {
                tempCurrentSpeed = (ShipInstance.Move.CurrentSpeed > 0) ? ShipInstance.Move.CurrentSpeed - ShipInstance.Move.BrakingForce : 0;
            }

            if (Math.Abs(tempCurrentSpeed - ShipInstance.Move.CurrentSpeed) > 0.1f)
            {
                ShipModel tempShipModel = new ShipModel(ShipInstance, ShipInstance.Health, 
                    new ShipMovomentModel(ShipInstance.Move.MaxSpeed, tempCurrentSpeed, ShipInstance.Move.AccelerationForce,
                        ShipInstance.Move.BrakingForce, ShipInstance.Move.YawPitchSpeed));

                ShipInstance = tempShipModel;   
            }
        }
        #endregion
    }
}