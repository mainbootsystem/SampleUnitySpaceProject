﻿using UnityEngine;

namespace Data.Script.Core.Ships
{
    public interface IFly
    {
        void FlyXZ(Vector2 direction);
        void FlyY(float direction);
        void Roll(float direction);
        void YawPitch(Vector2 delta);
        void SpeedControll(Vector2 xzDirection, float yDirection);
    }
}
