﻿using Data.Script.Core.Model;

namespace Data.Script.Core.Ships
{
    public interface IShip
    {
        ShipModel ShipInstance { get; }
        void InitializationShip();
    }
}
