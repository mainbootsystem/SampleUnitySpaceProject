﻿namespace Data.Script.Core
{
    public interface IBaseUpdater
    {
        bool IsActive { get; set; }

        void BaseAwake();
        void BaseUpdate();
        void BaseFixUpdate();
        void BaseLateUpdate();

        void SetActive(bool isFlag);
    }
}
