﻿using Data.Script.Core.Scene;
using System.Collections;
using Data.Script.Inputs;
using UnityEngine;

namespace Data.Script.Core.Scenes
{
    public class SceneManager : MonoBehaviour
    {
        [SerializeField] private TypeLoad type;
        [SerializeField] private ScenesBuilderDataPath builderDataPath;
        [SerializeField] private InputManager _inputManager;
        [SerializeField] private GamePause _gamePause;
        private ObjectPool _objectPool;
        private bool isSceneWasLoaded;
        private static SceneManager _instance;

        #region Propertys

        public static SceneManager Instance
        {
            get => _instance;
            private set => _instance = value;
        }
        public ObjectPool getObjectPool => _objectPool;
        public InputManager Inputs => _inputManager;
        public GamePause Pause => _gamePause;

        #endregion

        private void Start()
        {
            Instance = this;
            _objectPool = new ObjectPool(this);
            
            StartCoroutine(Initilization());
        }

        private void Update()
        {
            if(!isSceneWasLoaded || getObjectPool.getUpdates.Count < 1) return;

            for (int i = 0; i < getObjectPool.getUpdates.Count; i++)
                if(getObjectPool.getUpdates[i].IsActive) getObjectPool.getUpdates[i].BaseUpdate();
        }
        
        private void FixedUpdate()
        {
            if(!isSceneWasLoaded || getObjectPool.getUpdates.Count < 1) return;

            for (int i = 0; i < getObjectPool.getUpdates.Count; i++)
                if(getObjectPool.getUpdates[i].IsActive) getObjectPool.getUpdates[i].BaseFixUpdate();
        }
        
        private void LateUpdate()
        {
            if(!isSceneWasLoaded || getObjectPool.getUpdates.Count < 1) return;

            for (int i = 0; i < getObjectPool.getUpdates.Count; i++)
                if(getObjectPool.getUpdates[i].IsActive) getObjectPool.getUpdates[i].BaseLateUpdate();
        }

        private IEnumerator Initilization()
        {
            switch (type)
            {
                case TypeLoad.Find:
                {
                    IBaseUpdater[] baseUpdaters = FindObjectsOfType<BaseObject>();

                    _objectPool.SetBaseUpdaters(baseUpdaters);

                    foreach (var obj in _objectPool.getUpdates)
                        obj.BaseAwake();
                }
                break;
                case TypeLoad.Build:
                default:
                {
                    SceneBuilder builder = new GameObject("Builder").AddComponent<SceneBuilder>();
                    
                    builder.BuildScenes(builderDataPath);
                    yield return new WaitUntil(() => builder.IsReady);
                    builder.DestroyBuilder();
                }
                break;
            }

            foreach (var obj in _objectPool.getUpdates)
                obj.SetActive(obj.IsActive);

            Pause.IsPaused = false;
            isSceneWasLoaded = true;
        }
        
        #if UNITY_EDITOR
        private void Reset()
        {
            _inputManager = FindObjectOfType<InputManager>();
            _gamePause = FindObjectOfType<GamePause>();
            
            UnityEditor.EditorUtility.SetDirty(this);
        }
        #endif
    }
}
