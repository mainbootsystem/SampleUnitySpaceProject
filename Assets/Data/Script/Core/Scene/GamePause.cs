﻿using Data.Script.Core.Scenes;
using Data.Script.Inputs;
using UnityEngine;

namespace Data.Script.Core.Scene
{
    public class GamePause : BaseObject, IPause
    {
        public bool IsPaused { get; set; }

        private void LateUpdate()
        {
            if (SceneManager.Instance.Inputs.GetEscape())
                Pause();
        }

        public void Pause()
        {
            IsPaused = !IsPaused;
            if (!IsPaused)
            {
                Time.timeScale = 1f;
            }
            else
            {
                Time.timeScale = 0f;
            }
        }
    }
}
