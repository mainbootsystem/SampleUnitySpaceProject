﻿using Data.Script.Core;
using Data.Script.Core.Inputs;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Data.Script.Inputs
{
    public class InputManager : BaseObject
    {
        private PlayerControl inputs;

        public Vector2 getMoveDirection => inputs.Player.Move.ReadValue<Vector2>();
        public float getMoveYDirection => inputs.Player.MoveY.ReadValue<float>();
        public Vector2 getDeltaDirection => inputs.Player.MouseMove.ReadValue<Vector2>();
        public float getRollDirection => inputs.Player.Roll.ReadValue<float>();

        public override void BaseAwake()
        {
            inputs = new PlayerControl();
            inputs.Enable();
        }

        public bool GetEscape() => InputSystem.GetDevice<Keyboard>().escapeKey.wasPressedThisFrame;

        public void SetActiveInputs(bool isFlag)
        {
            if (isFlag) inputs.Enable();
            else inputs.Disable();
        }
    }
}
